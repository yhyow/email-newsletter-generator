module Constants
  USER_AGENT = "annotate_google; http://ponderer.org/download/annotate_google.user.js"
  NEWSLETTER_INTRO = %Q{
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <!doctype html>
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta property="og:title" content="*|MC:SUBJECT|*">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Sans:400,400italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <title>High Net Worth</title>

    <style type="text/css">
    		body{
    			margin:0;
    			padding:0;
    		}
    		h1{
    			font-family:'Josefin Sans', sans-serif;
    			font-size:36px;
    			font-weight:normal;
    			line-height:44px;
    		}
    		h2{
    			font-family:'Josefin Sans', sans-serif;
    			font-size:16px;
    			font-weight:normal;
    		}
    		h3{
    			font-family:'Montserrat', sans-serif;
    			font-size:16px;
    			font-weight:normal;
    			letter-spacing:1px;
    		}
    		h4{
    			font-family:'Montserrat', sans-serif;
    			font-size:14px;
    			font-weight:normal;
    			letter-spacing:1px;
    		}
    		.blue-texts{
    			color:#5a809a;
    		}
    		.red-texts{
    			color:#ac5954;
    		}
    		img{
    			border:0;
    		}
    		div.wrapper{
    			width:600px;
    			height:auto;
    			overflow:auto;
    			background-color:#1c1c1c;
    			color:#f4f1e0;
    			margin:0 auto;
    		}
    		div.full{
    			width:600px;
    			text-align:center;
    		}
    		div.half{
    			width:600px;
    			height:auto;
    			overflow:auto;
    			text-align:center;
    			margin:0 0 40px 0;
    		}
    		img.full{
    			width:600px;
    		}
    		img.half{
    			width:240px;
    		}
    		div.full-content{
    			width:520px;
    			padding:40px;
    			height:auto;
    			overflow:auto;
    		}
    		div.half-content{
    			width:240px;
    			height:auto;
    			overflow:auto;
    			margin-left:40px;
    			float:left;
    			display:inline-block;
    		}
    		div.full-content a{
    			color:#f4f1e0;
    			text-decoration:none;
    		}
    		div.half-content a{
    			color:#f4f1e0;
    			text-decoration:none;
    			line-height:150%;
    		}
    		div.company-list{
    			font-family:'Josefin Sans', sans-serif;
    			font-size:16px;
    			font-weight:normal;
    			width:240px;
    			height:auto;
    			overflow:auto;
    			margin:20px 0 20px 0;
    			line-height:150%;
    			text-align:center;
    		}
    		div.company-info{
    			width:199px;
    			float:left;
    		}
    		img.icon-picks{
    			width:41px;
    			height:41px;
    			float:right;
    		}
    		.vertdivider{
    			border-left:1px solid #828282;
    		}
    		.hnworth-logo{
    			margin:0 auto 40px auto;
    		}
    		.experience-button{
    			margin:40px auto 0 auto;
    		}
    		div.full-content p{
    			font-family:Georgia, serif;
    			font-size:16px;
    			font-weight:normal;
    			line-height:150%;
    		}
    		hr.divider{
    			color:#828282;
    			background-color:#828282;
    			height:1px;
    			margin:40px;
    			font-size:0;
    			border:0;
    		}
    		div.social{
    			padding:40px;
    			margin:0 auto;
    			width:520px;
    			display:block;
    			text-align:center;
    		}
    		div.social img{
    			width:36px;
    			height:36px;
    			display:inline-block;
    			margin:0 10px;
    		}
    		div.aboutus{
    			padding:40px;
    			margin:0 auto;
    			width:520px;
    			display:block;
    			text-align:center;
    		}
    		div.footer{
    			width:520px;
    			padding:40px;
    			height:auto;
    			overflow:auto;
    			background-color:#fff;
    			text-align:center;
    		}
    		.footer-logo-link{
    			width:100%;
    			display:block;
    		}
    		.footer-logo{
    			margin:0 auto 20px auto;
    			clear:both;
    		}
    		.footer-link{
    			margin:0 auto 20px auto;
    			font-family:'Montserrat', sans-serif;
    			font-size:14px;
    			font-weight:normal;
    			color:#0f0f0f;
    			text-decoration:none; 
    			font-family:'Josefin Sans', sans-serif;
    			font-size:13px;
    			font-weight:normal;
    			color:#0f0f0f;
    			text-decoration:none;
    			display:inline-block;
    		}
    		.footer-link{
    			margin:0 auto 20px auto;
    			font-family:'Montserrat', sans-serif;
    			font-size:14px;
    			font-weight:normal;
    			color:#0f0f0f;
    			text-decoration:none;
    			width:100%;
    		}
        #ad{
        width: 600px
      
        }
    </style></head>

    <body>
      <div class="wrapper">
  }
  
  NEWSLETTER_INTRO_2 = %Q{
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <!doctype html>
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta property="og:title" content="*|MC:SUBJECT|*">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Sans:400,400italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <title>High Net Worth</title>

    <style type="text/css">
    		body{
    			margin:0;
    			padding:0;
    		}
    		h1{
    			font-family:'Josefin Sans', sans-serif;
    			font-size:36px;
    			font-weight:normal;
    			line-height:44px;
    		}
    		h2{
    			font-family:'Josefin Sans', sans-serif;
    			font-size:16px;
    			font-weight:normal;
    		}
    		h3{
    			font-family:'Montserrat', sans-serif;
    			font-size:16px;
    			font-weight:normal;
    			letter-spacing:1px;
    		}
    		h4{
    			font-family:'Montserrat', sans-serif;
    			font-size:14px;
    			font-weight:normal;
    			letter-spacing:1px;
    		}
    		.blue-texts{
    			color:#5a809a;
    		}
    		.red-texts{
    			color:#ac5954;
    		}
    		img{
    			border:0;
    		}
    		div.wrapper{
    			width:600px;
    			height:auto;
    			overflow:auto;
    			background-color:#1c1c1c;
    			color:#f4f1e0;
    			margin:0 auto;
    		}
    		div.full{
    			width:600px;
    			text-align:center;
    		}
    		div.half{
    			width:600px;
    			height:auto;
    			overflow:auto;
    			text-align:center;
    			margin:0 0 40px 0;
    		}
    		img.full{
    			width:600px;
    		}
    		img.half{
    			width:240px;
    		}
    		div.full-content{
    			width:520px;
    			padding:40px;
    			height:auto;
    			overflow:auto;
    		}
    		div.half-content{
    			width:240px;
    			height:auto;
    			overflow:auto;
    			margin-left:40px;
    			float:left;
    			display:inline-block;
    		}
    		div.full-content a{
    			color:#f4f1e0;
    			text-decoration:none;
    		}
    		div.half-content a{
    			color:#f4f1e0;
    			text-decoration:none;
    			line-height:150%;
    		}
    		div.company-list{
    			font-family:'Josefin Sans', sans-serif;
    			font-size:16px;
    			font-weight:normal;
    			width:240px;
    			height:auto;
    			overflow:auto;
    			margin:20px 0 20px 0;
    			line-height:150%;
    			text-align:center;
    		}
    		div.company-info{
    			width:199px;
    			float:left;
    		}
    		img.icon-picks{
    			width:41px;
    			height:41px;
    			float:right;
    		}
    		.vertdivider{
    			border-left:1px solid #828282;
    		}
    		.hnworth-logo{
    			margin:0 auto 40px auto;
    		}
    		.experience-button{
    			margin:40px auto 0 auto;
    		}
    		div.full-content p{
    			font-family:Georgia, serif;
    			font-size:16px;
    			font-weight:normal;
    			line-height:150%;
    		}
    		hr.divider{
    			color:#828282;
    			background-color:#828282;
    			height:1px;
    			margin:40px;
    			font-size:0;
    			border:0;
    		}
    		div.social{
    			padding:40px;
    			margin:0 auto;
    			width:520px;
    			display:block;
    			text-align:center;
    		}
    		div.social img{
    			width:36px;
    			height:36px;
    			display:inline-block;
    			margin:0 10px;
    		}
    		div.aboutus{
    			padding:40px;
    			margin:0 auto;
    			width:520px;
    			display:block;
    			text-align:center;
    		}
    		div.footer{
    			width:520px;
    			padding:40px;
    			height:auto;
    			overflow:auto;
    			background-color:#fff;
    			text-align:center;
    		}
    		.footer-logo-link{
    			width:100%;
    			display:block;
    		}
    		.footer-logo{
    			margin:0 auto 20px auto;
    			clear:both;
    		}
    		.footer-link{
    			margin:0 auto 20px auto;
    			font-family:'Montserrat', sans-serif;
    			font-size:14px;
    			font-weight:normal;
    			color:#0f0f0f;
    			text-decoration:none; 
    			font-family:'Josefin Sans', sans-serif;
    			font-size:13px;
    			font-weight:normal;
    			color:#0f0f0f;
    			text-decoration:none;
    			display:inline-block;
    		}
    		.footer-link{
    			margin:0 auto 20px auto;
    			font-family:'Montserrat', sans-serif;
    			font-size:14px;
    			font-weight:normal;
    			color:#0f0f0f;
    			text-decoration:none;
    			width:100%;
    		}
        #ad{
        width: 600px
      
        }
    </style></head>

    <body>
    
    <div class="wrapper">
   <a href="http://www.hnworth.com/article-category/spend/experiences/"><img id="ad" src="http://www.hnworth.com/wp-content/uploads/2016/06/Gif-Banner_Final.gif"/></a>
    </div>
    
      <div class="wrapper">
  }

  NEWSLETTER_STORY_END = %Q{

    <div class="half" mc:hideable>
      <blockquote>
        <hr class="divider">
      </blockquote>
      <div class="half-content">
        <blockquote>
          <h3>Broker's Picks</h3>
        </blockquote>
        <div class="company-list" mc:repeatable>
          <div mc:edit="bp_item">

  }

  NEWSLETTER_BP_END = %Q{

      </div>
    </div>
  </div>

  <div class="half-content vertdivider">
    <blockquote>
      <h3>Listed Companies</h3>
    </blockquote>
    <div class="company-list" mc:repeatable>

  }

  NEWSLETTER_END = %Q{

      </div>
    </div>
  </div>
 
    <!-- social -->
        <div class="aboutus"> 
          <blockquote>
            <h4><a style="color: #f4f1e0;"href="http://www.hnworth.com/about/">About High Net Worth</a></h4>
      
          </blockquote>
        </div>
  
  
        <div class="social">
          <blockquote>
            <p><a href="http://www.hnworth.tumblr.com/" target="_blank" alt="Follow Our Tumblr Blog"><img src="https://gallery.mailchimp.com/2e084b49f52e5f3f21c970d3e/images/7ece0763-03de-45cf-b27b-d859349790d4.png"></a> 
              <a href="http://facebook.com/Hnworthcom" target="_blank" alt="Follow Us on Facebook"><img src="https://gallery.mailchimp.com/2e084b49f52e5f3f21c970d3e/images/8da33081-f6b1-4b4d-acc8-150a051aded3.png"></a> 
              <a href="http://instagram.com/hnworth" target="_blank" alt="Follow Us on Instagram"><img src="https://gallery.mailchimp.com/2e084b49f52e5f3f21c970d3e/images/1bdf8560-6647-4a63-b578-8a048c076c27.png"></a> 
            </p>
          </blockquote>
        </div>
  
        <div class="footer">
            <blockquote>
              <p><a href="http://www.hnworth.com" target="_blank" alt="High New Worth" class="footer-logo-link"><img class="images/footer-logo" src="https://gallery.mailchimp.com/2e084b49f52e5f3f21c970d3e/images/645a1f6b-c9d1-4ff7-b9cf-3d47946557ab.png"></a> 
                <a href="http://www.hnworth.com" target="_blank" alt="High New Worth" class="footer-link">www.hnworth.com</a>
              </p>
            </blockquote>
            <div class="footer-misc"> 
              <blockquote>
                <p><a href="mailto:editorial@hnworth.com" target="_blank">editorial@hnworth.com</a> <a href="file:///Macintosh HD/Users/allenlamwenchieh/Library/Containers/com.apple.mail/Data/Library/Mail Downloads/FD182ED8-C528-4D1B-8172-E0828C669FF7/*|UNSUB|*" target="_blank">To unsubscribe, click here</a> <a>© 2015 High Net Worth  All rights reserved.</a> 
                </p>
              </blockquote>
            </div>
         </div>

    </div>
    </body>
    </html>
 
  }
  
  EXPERIENCES = %Q{
    
	 <div id="featured">
	             <div class="full" mc:edit="featured_article">
	               <div class="full-content"> 
	                 <h1>Purchase Exclusive Experiences Now!</h1>
           
            
	               </div>
	             </div>
	           </div>

       
	            <div id="featured">
	             <div class="full" mc:edit="featured_article"><a href="http://www.hnworth.com/article/2015/10/01/coffee-and-a-personal-cello-masterclass/" target="_blank"><img class="full" src="http://www.hnworth.com/wp-content/uploads/2015/09/PeiSian_Hero_1400x935-1400x935.jpg"></a>
	               <div class="full-content"> 
	                 <h4>Experience</h4>
	                 <h1><a href="http://www.hnworth.com/article/2015/10/01/coffee-and-a-personal-cello-masterclass/" target="_blank">Coffee and a Personal Cello Masterclass</a></h1>
	                 <p>"Being in control is an art – every note that I play has to be intentional, not accidental."</p>
	               </div>
	             </div>
	           </div>
	 
	           <div id="featured">
	             <div class="full" mc:edit="featured_article"><a href="http://www.hnworth.com/article/2015/08/31/kevin-seah-bespoke-tailoring-experience/" target="_blank"><img class="full" src="http://www.hnworth.com/wp-content/uploads/2015/09/kevin_L2_handedit-1400x935.jpg"></a>
	               <div class="full-content"> 
	                 <h4>Experience</h4>
	                 <h1><a href="http://www.hnworth.com/article/2015/08/31/kevin-seah-bespoke-tailoring-experience/" target="_blank">
	 Kevin Seah’s Bespoke Tailoring Experience</a></h1>
	                 <p>"I want to show the world that a Singaporean tailor is just as capable or even better than an Italian tailor or a Savile Row tailor."</p>
	               </div>
	             </div>
	           </div>
  
  
  
  
  
	       <div id="featured">
	             <div class="full" mc:edit="featured_article"><a href="http://www.hnworth.com/article/2015/10/19/business-luncheon-and-tour-with-clinton-ang/" target="_blank"><img class="full" src="http://www.hnworth.com/wp-content/uploads/2015/12/Clinton-Ang-1500x800-exp-1400x747.jpg"></a>
	               <div class="full-content"> 
	                 <h4>Experience</h4>
	                 <h1><a href="http://www.hnworth.com/article/2015/10/19/business-luncheon-and-tour-with-clinton-ang/" target="_blank">Business Luncheon and Tour with Clinton Ang</a></h1>
	                 <p>"Everyone knows our company in the region because we are the oldest surviving wine merchants."</p>
	               </div>
	             </div>
	           </div>
	 
	           <div id="featured">
	             <div class="full" mc:edit="featured_article"><a href="http://www.hnworth.com/article/2015/11/13/darrell-ang-orchestra-experience/" target="_blank"><img class="full" src="http://www.hnworth.com/wp-content/uploads/2015/11/Darrell-Ang-Experience-Hero3-1400x934.jpg"></a>
	               <div class="full-content">
	                 <h4>Experience</h4>
	                 <h1><a href="http://www.hnworth.com/article/2015/11/13/darrell-ang-orchestra-experience/" target="_blank">
	 Conduct an Orchestra with Darrell Ang</a></h1>
	                 <p>"Conducting an orchestra and making music with people allowed me to connect more with music and that created a special feeling."</p>
	               </div>
	             </div>
	           </div>
    
    
  }
  
  INTRO_ART = %Q{
  
    /**************************************         WELCOME TO         ***********************************************
     *    .______        ___       __       __       _______ .______          _______  _______  .___  ___. 
     *    |   _  \      /   \     |  |     |  |     |   ____||   _  \        |   ____||       \ |   \/   | 
     *    |  |_)  |    /  ^  \    |  |     |  |     |  |__   |  |_)  |       |  |__   |  .--.  ||  \  /  | 
     *    |   _  <    /  /_\  \   |  |     |  |     |   __|  |      /        |   __|  |  |  |  ||  |\/|  | 
     *    |  |_)  |  /  _____  \  |  `----.|  `----.|  |____ |  |\  \----.   |  |____ |  '--'  ||  |  |  | 
     *    |______/  /__/     \__\ |_______||_______||_______|| _| `._____|   |_______||_______/ |__|  |__| 
     *                                                                                                    
     */***********************************          WELCOME TO         ***********************************************
  
  }
  
end

