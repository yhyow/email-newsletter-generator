require_relative 'data_loader'
require_relative '../constants'

class Newsletter
  attr_accessor :html, :file_name, :permission
  def initialize
    @html = ""
    @file_name = ""
    @permission = false
  end

  def build
    puts Constants::INTRO_ART
    content = DataLoader.new
    content.stage
    @html << Constants::NEWSLETTER_INTRO
    @html << content.build_stories(content.story_data)
    # @html << Constants::EXPERIENCES
    @html << Constants::NEWSLETTER_STORY_END
    @html << content.build_bp(content.bp_data)
    @html << Constants::NEWSLETTER_BP_END
    @html << content.build_lc(content.lc_data)
    @html << Constants::NEWSLETTER_END
    create_html
  end
  
  def create_html
    name = Time.new.strftime("%b%d%Y")
    file_name = "newsletter_"+name+".html"
    file = File.open(file_name, 'a:UTF-8') do |file|
      file.write(@html)
    end
    if file
      puts "EDM generated."
      email?
    else
      raise "There seems to be an issue with generating the HTML file."
    end
  end
  
  def email?
    puts "Email the last generated html file to the your colleugues? Enter y or n"
    response = gets.chomp
    case response
      when "y"
        puts "Sending ..."
        # send_email
      when "n"
        puts "Okay, if you say so ..."
        return
      else
        puts "Whut?? I don't understand. Please enter y or n only."
    end
  end

  # def send_email
 #
 #    Mail.defaults do
 #      delivery_method :smtp, address: "hnworth.com", port: 587
 #    end
 #
 #    mail = Mail.new do
 #     
 #    end
 #    mail.deliver!
 #    puts "Email with attachment has been sent. Goodbye!"
 #  end
end