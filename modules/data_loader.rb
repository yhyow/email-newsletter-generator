require_relative '../entries'
require_relative '../constants'
require_relative 'newsletter'
require_relative 'url'
require_relative 'hp_url'
require_relative 'trend_url'
require_relative 'story_url'
require_relative 'lc_url'
require_relative 'bp_url'

class DataLoader
  attr_accessor :story_data, :bp_data, :lc_data

  def initialize
    @@url_instances = []
    @story_data = []
    @bp_data = []
    @lc_data = []
    
    if not URLS::HP_URLS.empty?
      URLS::HP_URLS.each{ |url| @@url_instances << Hp_url.new(url) }
    end
    
    if not URLS::TREND_URLS.empty?
      URLS::TREND_URLS.each{ |url| @@url_instances << Trend_url.new(url) }
    end
    
    if not URLS::STORY_URLS.empty?
      URLS::STORY_URLS.each{ |url| @@url_instances << Story_url.new(url) }
    end
    
    if not URLS::BP_URLS.empty?
      URLS::BP_URLS.each{ |url| @@url_instances << Bp_url.new(url) }
    end
    
    if not URLS::LC_URLS.empty?
      URLS::LC_URLS.each{ |url| @@url_instances << Lc_url.new(url) }
    end
    p @@url_instances
  end
  
  def stage
    @@url_instances.each do |url_instance|
      # This populates entries for each url object with scrapped data
      url_instance.process
      
      case url_instance.class.to_s
        when "Hp_url"
          @story_data << url_instance.entries
        when "Trend_url"
          @story_data << url_instance.entries
        when "Story_url"
          @story_data << url_instance.entries
        when "Bp_url"
          @bp_data << url_instance.entries
        when "Lc_url"
          @lc_data << url_instance.entries
        else
          raise "#{url_instance.class} is unknown to us."
      end
    end
  end

  def build_lc(lc_data)
    story_snippets = ""
    lc_data.each do |entry|
      story_snippets << %Q{
        <div mc:edit="lc_item">
          <blockquote>
            <p><a href="#{entry[1]}">#{entry[0]}</a><br>
              <span class="red-texts"></span> 
              </p>
          </blockquote>
        </div>
      }
    end
    story_snippets
  end
  
  def build_bp(bp_data)
    story_snippets = ""
    bp_data.each do |entry|
      story_snippets << %Q{
        <div class="company-info">
          <blockquote>
            <p><a href="#{entry[1]}">#{entry[0]}</a><br />
            </p>
          </blockquote>
        </div>
      }
    end
    story_snippets
  end
  
  def build_stories(story_data)
    story_snippets = ""
    story_data.each_with_index do |entry,i|
      if i == 0 
        story_snippets << %Q{
          <div id="featured">
            <div class="full" mc:edit="featured_article"><a href="#{entry[4]}" target="_blank"><img class="full" src="#{entry[2]}"></a>
              <div class="full-content"> <a href="#{entry[4]}" target="_blank" alt="High New Worth"><img class="hnworth-logo" src="https://gallery.mailchimp.com/2e084b49f52e5f3f21c970d3e/images/db8e50dd-458f-4493-9b33-d80a9b83544e.png"></a>
                <h4>#{entry[3]}</h4>
                <h1><a href="#{entry[4]}" target="_blank">#{entry[0]}</a></h1>
                <p>#{entry[1]}</p>
              </div>
            </div>
          </div>
        } 
      else
        story_snippets << %Q{
          <div id="featured">
            <div class="full" mc:edit="featured_article"><a href="#{entry[4]}" target="_blank"><img class="full" src="#{entry[2]}"></a>
              <div class="full-content"> 
                <h4>#{entry[3]}</h4>
                <h1><a href="#{entry[4]}" target="_blank">#{entry[0]}</a></h1>
                <p>#{entry[1]}</p>
              </div>
            </div>
          </div>
        } 
      end
    end
    story_snippets
  end
end