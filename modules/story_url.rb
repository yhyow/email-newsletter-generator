require_relative 'url'

class Story_url < Url
  def initialize(name)
    super
  end
  
  def process
    doc = Nokogiri::HTML(open(name, 'User-Agent' => Constants::USER_AGENT), nil, "UTF-8")

    doc.xpath("//h1").each_with_index do |link,i|
    @entries << link.text if i == 3 #title
    end

    doc.xpath("//p").each_with_index do |link,i|
    @entries << link.text if i == 2 #content
    end

    doc.xpath("//div[@class='banner']/@style").each_with_index do |link,i|
    @entries << link.to_s[23..-3] #image
    end

    doc.xpath("//h1/a").each_with_index do |link,i|
    @entries << link.text #cat
    end

    @entries << name

    puts 'waiting ...'
    sleep 7
  end
end