require_relative 'url'

class Trend_url < Url
  def initialize(name)
    super
  end
  
  def process
    doc = Nokogiri::HTML(open(name, 'User-Agent' => Constants::USER_AGENT), nil, "UTF-8")

    doc.xpath("//h1").each_with_index do |link,i|
    @entries << link.text if i == 2 #conversations with ...
    end

    doc.xpath("//h4").each_with_index do |link,i| #Intro
    @entries << link.text if i == 1
    end

    doc.xpath("//img/@src").each_with_index do |link,i| #Image
    @entries << link if i == 54 #image
    end

    doc.css(".rotated-title").each_with_index do |link,i|
    @entries << link.text #cat
    end

    entries << name

    puts 'waiting ...'
    sleep 6
  end
  
end